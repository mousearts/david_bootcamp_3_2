import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

   roomList : Object [] = [
    { "room_number": "101", "room_rate": 2000000, "status" : 1 },
    { "room_number": "102", "room_rate": 2000000, "status" : 1 },
    { "room_number": "103", "room_rate": 2000000, "status" : 0 },
    { "room_number": "104", "room_rate": 2000000, "status" : 1 },
    { "room_number": "201", "room_rate": 4000000, "status" : 0 },
    { "room_number": "202", "room_rate": 4000000, "status" : 0 },
    { "room_number": "203", "room_rate": 4000000, "status" : 1 },
    { "room_number": "204", "room_rate": 4000000, "status" : 1 },

  ]


  constructor() { 
    
  }

  ngOnInit() {
  }

}
