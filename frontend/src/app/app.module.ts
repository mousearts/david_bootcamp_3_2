import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoomListComponent } from './components/room-list/room-list.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { CustomerRegistrationComponent } from './components/customer-registration/customer-registration.component';
import { RoomBookingComponent } from './components/room-booking/room-booking.component';
import { AssetComponent } from './components/asset/asset.component';
import { CustomerListComponent } from './components/customer-list/customer-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomListComponent,
    NavigationComponent,
    CustomerRegistrationComponent,
    RoomBookingComponent,
    AssetComponent,
    CustomerListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
