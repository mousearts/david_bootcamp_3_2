<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Booking;

class BookingController extends Controller
{
    function addBooking(Request $request){
        DB::beginTransaction();
        try{

 
            $room_id = $request -> input('room_id');
            $customer_id = $request -> input('customer_id');
            $paid_status = $request -> input('paid_status');
            $check_in = $request -> input ('check_in');


            $booking = new Booking;
            $booking -> room_id = $room_id;
            $booking -> customer_id = $customer_id;
            $booking -> paid_status = $paid_status;
            $booking -> check_in = $check_in;
            $booking -> save();

            

            DB::commit(); //commit untuk masuk kedalam database bila berhasil

        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
            
        }
    }
}
